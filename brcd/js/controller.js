var brocodeControllers= angular.module('brocodeControllers', ['brocodeService']);

brocodeControllers.controller('BrocodeListCtrl', ['$scope', 'BrocodeAllArticles', 'BrocodeRandomArticle', function BrocodeListCtrl($scope, BrocodeAllArticles, BrocodeRandomArticle) {

	$scope.random = function() {
		BrocodeRandomArticle.query(function(response) {
			$scope.brocodeRules = new Array();
			$scope.brocodeRules.push(response);
		});
	};

	$scope.all = function() {
		BrocodeAllArticles.query(function(response) {
			$scope.brocodeRules = response;
		});
	};

	$scope.all();

	$scope.orderProp = 'number';
}]);

brocodeControllers.controller('BrocodeDetailCtrl', ['$scope', '$routeParams', 'BrocodeArticle', function BrocodeDetailCtrl($scope, $routeParams, BrocodeArticle) {
	BrocodeArticle.get({brocodeId:$routeParams.brocodeId}, function(brocode) {
		$scope.brocode = brocode;
	});

}]);


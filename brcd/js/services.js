var brocodeService = angular.module('brocodeService', ['ngResource'])
	.factory('BrocodeAllArticles', function($resource){
		return $resource('https://api.christophhaefner.de/brocode/all/', {});
	})
	.factory('BrocodeArticle', function($resource){
		return $resource('https://api.christophhaefner.de/brocode/article/:brocodeId', {brocodeId:'@id'});
	})
	.factory('BrocodeRandomArticle', function($resource){
		return $resource('https://api.christophhaefner.de/brocode/random/', {},
			{ query: {method:'GET', isArray: false}
		});
	});


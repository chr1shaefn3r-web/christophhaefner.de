var brocodeApp = angular.module('brocodeApp', [
	'brocodeService',
	'ngRoute',
	'brocodeControllers'
]);

brocodeApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'partials/brocode-list.html',
		controller: 'BrocodeListCtrl'
	})
	.when('/:brocodeId', {
		templateUrl: 'partials/brocode-detail.html',
		controller: 'BrocodeDetailCtrl'
	})
	.otherwise({
		redirectTo: '/'
	});
}]);

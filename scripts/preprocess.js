#!/usr/bin/env node
'use strict';

var pp = require('preprocess'),
	cb = function (err) {
		if (err) console.error(err);
	};

const firstLevel = {
	RELATIVE_PATH: ".."
};
pp.preprocessFile('build/projects/index.html', 'build/projects/index.html', firstLevel, cb);
pp.preprocessFile('build/projects/archive.html', 'build/projects/archive.html', firstLevel, cb);
pp.preprocessFile('build/3dprinting/index.html', 'build/3dprinting/index.html', firstLevel, cb);
pp.preprocessFile('build/events/index.html', 'build/events/index.html', firstLevel, cb);
pp.preprocessFile('build/brcd/index.html', 'build/brcd/index.html', firstLevel, cb);
pp.preprocessFile('build/badges/index.html', 'build/badges/index.html', firstLevel, cb);

const secondLevel = {
	RELATIVE_PATH: "../.."
};
pp.preprocessFile('build/events/archive/2014.html', 'build/events/archive/2014.html', secondLevel, cb);
pp.preprocessFile('build/events/archive/2015.html', 'build/events/archive/2015.html', secondLevel, cb);
pp.preprocessFile('build/events/archive/2016.html', 'build/events/archive/2016.html', secondLevel, cb);
pp.preprocessFile('build/events/archive/2017.html', 'build/events/archive/2017.html', secondLevel, cb);
